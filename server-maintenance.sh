#!/usr/bin/env sh

function manage_packages() {

    clear
    printf "Uitvoeren van update package registries\n"
    sudo apt-get update
    # Log package statusses
    sudo apt-get upgrade
    sudo apt-get autoremove

    wait_for_keypress "Packages updated"

}

function configure_iptables() {
    
    clear 
    printf "Configuring firewall rules.\n"

    printf "Check if force SYN packets rule is present\n"
    sudo iptables -C INPUT -p tcp ! --syn -m state --state NEW -j DROP
    if [ $? = 1 ] ; then
        printf "Adding SYNC packets rule\n"
        sudo iptables -A INPUT -p tcp ! --syn -m state --state NEW -j DROP
    else 
        printf "Rule already present\n"
    fi

    printf "Check if XMAS packets are dropped\n"
    sudo iptables -C INPUT -p tcp --tcp-flags ALL ALL -j DROP
    if [ $? = 1 ] ; then
        printf "Drop XMAS packets\n"
        sudo iptables -A INPUT -p tcp --tcp-flags ALL ALL -j DROP
    else
        printf "Rule already present\n"
    fi

    printf "Check if null packets are dropped\n"
    sudo iptables -C INPUT -p tcp --tcp-flags ALL NONE -j DROP
    if [ $? = 1 ] ; then
        printf "Drop null packets\n"
        sudo iptables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP
    else
        printf "Rule already present\n"
    fi

    printf "Check if incomming packets with fragments\n"
    sudo iptables -C INPUT -f -j DROP
    if [ $? = 1 ] ; then
        printf "Drop incomming packtes with fragments\n"
        sudo iptables -A INPUT -f -j DROP
    else
        printf "Rule already present\n"
    fi

    wait_for_keypress "Firewall rules checked"
}

function configure_sysctl_securely() {
    configureSysctl=1
    while [ $configureSysctl = 1 ] ; do
        printf "open file /etc/sysctl.conf\n"
        printf "Check following (take a printscreen):\n"
        printf "\tnet.ipv4.ip_forward should be set to '0' to disable IP forwarding\n"
        printf "\tnet.ipv4.conf.all.send_redirects and net.ipv4.conf.default.send_redirects set to '0' to disable the Send Packet Redirects.\n"
        printf "\tnet.ipv4.conf.all.accept_redirects and net.ipv4.conf.default.accept_redirects set to '0' to disable ICMP Redirect Acceptance.\n"
        printf "\tnet.ipv4.icmp_ignore_bogus_error_responses set to '1' to enable Bad Error Message Protection.\n"
        wait_for_keypress "Configure sysctl.conf"
        
        sudo nano /etc/sysctl.conf
        
        printf "File correct configured?\n"
        select choice in Yes No; do 
            case $choice in
                Yes ) configureSysctl=0; break;;
                No ) break;;
            esac
        done
    done

    wait_for_keypress "File correct configured"
}

function configure_fail2ban() {

    clear
    sudo apt-get install fail2ban

    sudo systemctl start fail2ban
    sudo systemctl enable fail2ban

    wait_for_keypress "bantime = 600; findtime = 600; maxretry = 5"

    sudo nano /etc/fail2ban/jail.local    

}

function configure_root_user_timeout() {

    clear
    printf "[\$UID -eq 0] && TMUOT=600"

    wait_for_keypress "Configure root user timeout"

    sudo nano /etc/profiles
}

function check_disk_usage() {
    df -h
}

function wait_for_keypress() {
    printf "\n"
    running=1
    while [ $running = 1 ] ; do
        read -t 3 -n 1
        if [ $? = 0 ] ; then
            running=0 ;
        else
            printf "%s - Waiting for the keypress\n" "$1"
        fi
    done
}

manage_packages
configure_iptables
configure_sysctl_securely
configure_fail2ban
configure_root_user_timeout
check_disk_usage