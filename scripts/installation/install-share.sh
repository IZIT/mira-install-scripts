#!/bin/bash

root=$HOME/mirabackend

share=
domain=
username=
password=

# Installs the cifs-utils package which is needed for mounting the share.
function install_cifs_utils(){

	case $($HOME/mirabackend/scripts/get-linux-dist.sh) in
	"Red Hat"*)
		sudo yum install cifs-utils
		;;
	"Boot2Docker"*)
		tce-load -wi cifs-utils
		;;
	*)
		sudo apt-get update
		sudo apt-get install cifs-utils
		;;
	esac
}

# Vragen naar de locatie van de share
function ask_for_share_location() {
	echo "Wat is het de locatie van de share. (//ipadres/map/map)?"
	loop=true
	wrong=false
	# Inlezen van de locatie
	read -r share
	while [[ "$loop" = true ]]; do
		if [[ "$wrong" = true ]]; then
			wrong=false
			echo "Wat is de correcte locatie? (//ipadres/map/map)"
			read -r share
		fi
		echo "Ben je zeker dat '$share' de juiste locatie is?"
		select yn in Yes No; do
			case $yn in
				Yes ) loop=false; run=false; break;;
				No ) wrong=true; break;;
			esac
		done
	done
}

function ask_for_domain() {
	echo "Wat is het domein van de gebruiker voor de share?"
	read -r domain
	loop=true
	wrong=false
	while [[ "$loop" = true ]]; do
		if [[ "$wrong" = true ]]; then
			wrong=false
			echo "Wat is het correcte domein?"
			read -r domain
		fi
		echo "Ben je zeker dat '$domain' het domein is voor de gebruiker van de share?"
		select yn in Yes No; do
			case $yn in
				Yes ) loop=false; run=false; break;;
				No ) wrong=true; break;;
			esac
		done
	done
}

function ask_for_username() {
	echo "Wat is de gebruikersnaam van de domein gebruiker?"
	read -r username
	loop=true
	wrong=false
	while [[ "$loop" = true ]]; do
		if [[ "$wrong" = true ]]; then
			wrong=false
			echo "Wat is de correcte gebruikersnaam van de domein gebruiker?"
			read -r username
		fi
		echo "Ben je zeker dat '$username' de gebruikersnaam is van de domein gebruiker?"
		select yn in Yes No; do
			case $yn in
				Yes ) loop=false; run=false; break;;
				No ) wrong=true; break;;
			esac
		done
	done
}

function ask_for_password() {
	run=true
	passConfirm=
	while [[ "$run" = true ]]; do
		echo "Geef het paswoord van de domein gebruiker."
		read -s password
		echo "Bevestig het password."
		read -s passConfirm
		if [[ "$password" != "$passConfirm" ]]; then
			echo "De paswoorden komen niet overeen. Probeer opnieuw."
		else
			run=false
		fi
	done
}

function print_title() {
	clear
	echo "-----------------------"
	echo "INSTELLEN VAN DE SHARE."
	echo "-----------------------"
	echo ""
}

# Install cifs utils before doing anything else to avoid having
# to re-enter all the information in case the installation fails.
install_cifs_utils

# calling functions to ask for parameters
print_title
ask_for_share_location
clear
print_title
ask_for_domain
clear
print_title
ask_for_username
clear
print_title
ask_for_password
clear
print_title

echo "Instellen van share."
sed -i \
	-e "s@\${shareDomain}@$domain@" \
	-e "s@\${shareLocation}@$share@" \
	-e "s@\${shareUsername}@$username@" \
	-e "s|\${sharePassword}|$password|" \
	$root/docker-files/docker-compose.yml

read -p "Press enter to continue"
