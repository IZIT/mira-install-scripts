#!/bin/bash

root=$HOME/mirabackend

##############
# PARAMETERS #
##############

RED='\033[0;31m'
NC='\033[0m'
DB_PORT=1433
ORGANISATION=
DNS_NAME=
BACKEND_NAME=
DB_SERVER=
DB_USER=
DB_PASSWORD=
INCLUDE_CERTIFICATE=0
CERT_NAME=
KEY_NAME=
DOCKER_CONFIG_LOCATION=

#############
# FUNCTIONS #
#############

function ask_for_organisation() {
  RUN=true
  FIRST_RUN=true
  while [[ "$RUN" = true ]]; do
    if [[ "$FIRST_RUN" = true ]]; then
      echo "What is the name of the organisation?"
      echo "This is the name you entered while making the license for the organisation."
      echo "Please make certain that the names correspond."
    fi
    read -r ORGANISATION
    FIRST_RUN=false
    ORGANISATION_LOOP=true
    WRONG_ORGANISATION=false
    while [[ "$ORGANISATION_LOOP" = true ]]; do
      if [[ "$WRONG_ORGANISATION" = true ]]; then
        WRONG_ORGANISATION=false
        echo "So, what is the correct name of the organisation?"
        read -r ORGANISATION
      fi
      echo "Are you certain that the name of the organisation is: '$ORGANISATION'?"
      select yn in Yes No; do
        case $yn in
          Yes ) ORGANISATION_LOOP=false; RUN=false; break;;
          No ) WRONG_ORGANISATION=true; break;;
        esac
      done
    done
  done
}

function ask_for_backend() {
  RUN=true
  FIRST_RUN=true
  while [[ "$RUN" = true ]]; do
    if [[ "$FIRST_RUN" = true ]]; then
      echo "What is the name of the backend?"
      echo "This is the name you entered while making the license for the organisation."
      echo "Please make certain that the names correspond."
    fi
    read -r BACKEND_NAME
    FIRST_RUN=false
    BACKEND_LOOP=true
    WRONG_BACKEND=false
    while [[ "$BACKEND_LOOP" = true ]]; do
      if [[ "$WRONG_BACKEND" = true ]]; then
        WRONG_BACKEND=false
        echo "So, what is the correct name of the backend?"
        read -r BACKEND_NAME
      fi
      echo "Are you certain that the name of the backend is: '$BACKEND_NAME'?"
      select yn in Yes No; do
        case $yn in
          Yes ) BACKEND_LOOP=false; RUN=false; break;;
          No )  WRONG_BACKEND=true; break;;
        esac
      done
    done
  done
}

function ask_for_server(){
  RUN=true
  FIRST_RUN=true
  while [[ "$RUN" = true ]]; do
    if [[ "$FIRST_RUN" = true ]]; then
      echo "Can you give me the location of the database?"
    fi
    read -r DB_SERVER
    FIRST_RUN=false
    SERVER_LOOP=true
    WRONG_SERVER=false
    while [[ "$SERVER_LOOP" = true ]]; do
      if [[ "$WRONG_SERVER" = true ]]; then
        WRONG_SERVER=false
        echo "So, what is the location of the database server?"
        read -r DB_SERVER
      fi
      echo "Are you certain that the location of the database is: '$DB_SERVER'?"
      select yn in Yes No; do
        case $yn in
          Yes ) SERVER_LOOP=false; RUN=false; break;;
          No ) WRONG_SERVER=true; break;;
        esac
      done
    done
  done
  clear
}

function ask_for_port() {
  RUN=true
  while [[ "$RUN" = true ]]; do
    echo "Can you give me the port to connect to on the database server?"
    select port in 1433 Other; do
      case $port in
        1433 ) DB_PORT=1433; break;;
        Other ) read -p "Portnumber: " DB_PORT; break;;
      esac
    done
    echo "Are you sure the port is: $DB_PORT"
    select option in Yes No; do
      case $option in
        Yes ) RUN=false; break;;
        No) break;;
      esac
    done
  done
}

function ask_for_user() {
  RUN=true
  FIRST_RUN=true
  while [[ "$RUN" = true ]]; do
    if [[ "$FIRST_RUN" = true ]]; then
      echo "Can you give me the username to use when trying to connect to the database server?"
    fi
    read -r DB_USER
    FIRST_RUN=false
    USER_LOOP=true
    WRONG_USER=false
    while [[ "$USER_LOOP" = true ]]; do
      if [[ "$WRONG_USER" = true ]]; then
        WRONG_USER=false
        echo "So, what is the username for the database server?"
        read -r DB_USER
      fi
      echo "Are you certain that the username for the databse is: '$DB_USER'?"
      select yn in Yes No; do
        case $yn in
          Yes ) USER_LOOP=false; RUN=false; break;;
          No ) WRONG_USER=true; break;;
        esac
      done
    done
  done
}

function ask_for_password() {
  RUN=true
  while [[ "$RUN" = true ]]; do
    echo "Can you give me the password for the database server?"
    read -s DB_PASSWORD
    echo "Please confirm the password by typing it again."
    read -s DB_PASSWORD_1
    if [[ "$DB_PASSWORD" != "$DB_PASSWORD_1" ]]; then
      echo "The entered passwords are not the same. Try again."
    else
      RUN=false
    fi
  done
}

function count_files(){ ls $1 | wc -l; }

function show_summary() {
  echo "Summary:"
  echo "We are installing this on a '$($HOME/mirabackend/scripts/get-linux-dist.sh)' machine."
  echo "The organisation is: '$ORGANISATION' and this backend will be called: '$BACKEND_NAME'."
  echo "The database is located at: '$DB_SERVER'."
  echo "The user will login with the username '$DB_USER' on port '$DB_PORT'."
}

function reset_parameters() {
  DB_PORT=1433
  ORGANISATION=
  BACKEND_NAME=
  DB_SERVER=
  DB_USER=
  DB_PASSWORD=
  DNS_NAME=
  INCLUDE_CERTIFICATE=0
  CERT_NAME=
  KEY_NAME=
  DOCKER_CONFIG_LOCATION=
}

clear
PARAMETERS_COMPLETE=false

while [[ "$PARAMETERS_COMPLETE" = false ]]; do
  ask_for_organisation
  clear
  ask_for_backend
  clear
  ask_for_server
  clear
  ask_for_port
  clear
  ask_for_user
  clear
  ask_for_password
  clear
  show_summary
  echo "Is this correct?"
  select option in Yes No; do
    case $option in
      Yes ) PARAMETERS_COMPLETE=true; break;;
      No) reset_parameters; break;;
    esac
  done
done

# docker netwerk aanmaken
docker network create mira-network

# Indien directory nog niet bestaat
if [[ ! -d "$HOME/.docker" ]]; then
  mkdir $HOME/.docker
fi

# login into docker
docker login registry.gitlab.com
DOCKER_CONFIG_LOCATION=$HOME/.docker/config.json

# parameters invullen in compose file
sed -i "s@\${organisation}@$ORGANISATION@" $root/docker-files/docker-compose.yml
sed -i "s@\${backendName}@$BACKEND_NAME@" $root/docker-files/docker-compose.yml
sed -i "s@\${dbServer}@${DB_SERVER/\\/\\\\\\\\}@" $root/docker-files/docker-compose.yml
sed -i "s@\${dbPort}@$DB_PORT@" $root/docker-files/docker-compose.yml
sed -i "s@\${dbUser}@$DB_USER@" $root/docker-files/docker-compose.yml
sed -i "s|\${dbPassword}|$DB_PASSWORD|" $root/docker-files/docker-compose.yml
sed -i "s@\${dockerHome}@$DOCKER_CONFIG_LOCATION@" $root/docker-files/docker-compose.yml

cat $root/docker-files/docker-compose.yml

echo "Is this generated compose file correct? Please double check!"
select option in Yes No; do
  case $option in
    Yes ) PROCEED=1; break;;
    No) echo -e "${RED}Please redo the installation and make sure you provide the correct parameters.${NC}" ; exit 0;;
    esac
done

if [[ "$PROCEED" = 1 ]]; then
  # script complete normally
  cd $HOME/mirabackend/docker-files

  DOCKER_COMPOSE_OPTIONS="up -d"

  case $($HOME/mirabackend/scripts/get-linux-dist.sh) in
  "Boot2Docker"*)
    # docker-compose may not be run as root, because the root user does not
    # have a config.json file, and hence no auth for registry.gitlab.com. Only
    # the 'docker' user has this file (under /home/docker/.docker/config.json).
    docker-compose ${DOCKER_COMPOSE_OPTIONS}
    ;;
  *)
    # ensure docker-compose works for all users, even root, although this is discouraged.
    sudo $(command -v docker-compose) ${DOCKER_COMPOSE_OPTIONS}
    ;;
  esac

  exit 0
fi
