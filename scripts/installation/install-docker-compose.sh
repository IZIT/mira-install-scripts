#!/bin/bash
# installeren van docker compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
# Executable permissie aan binaries
sudo chmod +x /usr/local/bin/docker-compose
