#!/bin/bash

function result() { [ -n "$1" ] && echo "$1" && exit 0; }

function get_linux_dist(){

	if [ -r /etc/lsb-release ]
	then
		result "$(grep 'DISTRIB_ID' /etc/lsb-release | sed 's/DISTRIB_ID=//' | head -1)"
	fi

	if [ -r /etc/os-release ]
	then
		result "$(source /etc/os-release; echo ${NAME})"
	fi

	echo "Unknown"
	exit 1
}

get_linux_dist
