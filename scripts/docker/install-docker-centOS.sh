#!/bin/bash

# install required packages
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
# Setup stable repository
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
# Installing docker
sudo yum install docker-ce docker-ce-cli containerd.io
# docker starten
sudo systemctl start docker
