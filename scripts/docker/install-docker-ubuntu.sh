#!/bin/bash

# update apt package
sudo apt-get update
# install packages to allow apt to use a repository over HTTPS
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
# Add Docker's official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
# Docker repository toevoegen aan apt package manager
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
# apt package updaten
sudo apt-get update
# docker installeren
sudo apt-get install docker-ce docker-ce-cli containerd.io
# verifiëren of docker geinstalleerd is.
sudo docker --version
# Post installation steps Docker for ubunut
# 2. Add user to the docker group
sudo usermod -aG docker $USER
# 3. Re-evalute group membership
newgrp docker

