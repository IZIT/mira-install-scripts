#!/bin/bash

# Nodig om dnf repositories te beheren vanuit command line
sudo dnf -y install dnf-plugins-core
# stable docker repo instellen
sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
# install docker
sudo dnf install docker-ce docker-ce-cli containerd.io
# start docker
sudo systemctl start docker