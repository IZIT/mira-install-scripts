#!/bin/bash

# update de apt package index
sudo apt-get update
# Packages toe te laten over HTTPS
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
# Add Dockers's officiele GPG key
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
# repo toevoegen
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
# update de apt package index
sudo apt-get update
# Laatste versie van docker installeren
sudo apt-get install docker-ce docker-ce-cli containerd.io
