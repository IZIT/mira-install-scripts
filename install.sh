#!/bin/bash

url=https://gitlab.com/IZIT/mira-install-scripts/raw/master
root=$HOME/mirabackend

function create_documents() {
	mkdir $root
	mkdir $root/cert-files
	mkdir $root/nginx-config
	mkdir $root/docker-files
	mkdir $root/scripts
	mkdir $root/scripts/docker
	mkdir $root/scripts/installation
}

function clean_documents() {
	rm -r $root/cert-files
	rm -r $root/nginx-config
	rm -r $root/scripts
}

function fetch_documents() {
	# Aanmaken van documenten
	create_documents

	# Ophalen van configuratie bestanden
	curl -o $root/docker-files/docker-compose.yml $url/docker-files/docker-compose.yml

	# Scripts voor installatie docker op juiste Linux distributie
	curl -o $root/scripts/docker/install-docker-centOS.sh $url/scripts/docker/install-docker-centOS.sh
	curl -o $root/scripts/docker/install-docker-debian.sh $url/scripts/docker/install-docker-debian.sh
	curl -o $root/scripts/docker/install-docker-fedora.sh $url/scripts/docker/install-docker-fedora.sh
	curl -o $root/scripts/docker/install-docker-ubuntu.sh $url/scripts/docker/install-docker-ubuntu.sh
	curl -o $root/scripts/docker/install-docker-redhat.sh $url/scripts/docker/install-docker-redhat.sh
	
	# Algemene scripts
	curl -o $root/scripts/get-linux-dist.sh $url/scripts/get-linux-dist.sh
	curl -o $root/scripts/installation/install-backend.sh $url/scripts/installation/install-backend.sh
	curl -o $root/scripts/installation/install-docker-compose.sh $url/scripts/installation/install-docker-compose.sh
	curl -o $root/scripts/installation/install-share.sh $url/scripts/installation/install-share.sh
}

function set_permissions() {
	chmod +x $root/scripts/docker/install-docker-centOS.sh
	chmod +x $root/scripts/docker/install-docker-debian.sh
	chmod +x $root/scripts/docker/install-docker-fedora.sh
	chmod +x $root/scripts/docker/install-docker-ubuntu.sh
	chmod +x $root/scripts/docker/install-docker-redhat.sh
	chmod +x $root/scripts/get-linux-dist.sh
	chmod +x $root/scripts/installation/install-backend.sh
	chmod +x $root/scripts/installation/install-docker-compose.sh
	chmod +x $root/scripts/installation/install-share.sh
}

function is_installed(){ [ -x "$(command -v $1)" ]; }

function install_docker(){

	# Install docker.
	if ! is_installed docker
	then
		case $($HOME/mirabackend/scripts/get-linux-dist.sh) in
		"Ubuntu") $root/scripts/docker/install-docker-ubuntu.sh;;
		"centOS") $root/scripts/docker/install-docker-centOS.sh;;
		"fedora") $root/scripts/docker/install-docker-fedora.sh;;
		"debian") $root/scripts/docker/install-docker-debian.sh;;
		"Red Hat Enterprise Linux") $root/scripts/docker/install-docker-redhat.sh;;
		esac
	fi

	# Ensure that docker is installed.
	if ! is_installed docker
	then
		echo "Error: docker is not installed."
		exit 1
	fi

	# Ensure that docker is running.
	if ! sudo docker ps &> /dev/null
	then
		echo "Error: docker is not running."
		exit 1
	fi

	# Install docker-compose.
	if ! is_installed docker-compose
	then
		$root/scripts/installation/install-docker-compose.sh
	fi

	# Ensure that docker-compose is installed.
	if ! is_installed docker-compose
	then
		echo "Error: docker-compose is not installed."
		exit 1
	fi
}

clear

echo "Welkom bij de installatie van de Mira backend."
echo "Ophalen van de nodige documenten."
# Documenten ophalen en progress bar laten zien aan gebruiker.
fetch_documents
set_permissions

install_docker

$root/scripts/installation/install-share.sh
$root/scripts/installation/install-backend.sh

clean_documents
