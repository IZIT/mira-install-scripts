# Mira Install scripts

Dit project bevat de nodige scripts om de volledige architectuur van Mira op te zetten op een Linux omgeving.

## Getting Started

Voor installatie moet zeker de [documentatie](https://gitlab.com/IZIT/mira-install-scripts/blob/master/Documentation/main.pdf) doorgenomen worden.

## Authors

* **Lennart Van Damme** - *Initial work* - [LennartIZIT](https://gitlab.com/LennartIZIT)

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

